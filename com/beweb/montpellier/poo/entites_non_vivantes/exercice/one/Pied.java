
package com.beweb.montpellier.poo.entites_non_vivantes.exercice.one;

public class Pied{

    // Propriétés de la class Matiere
    Integer hauteur;
    
    // Constructeur par defaut
    public Pied(){
    this.hauteur = 0;
    }
    
    // Constructeur avec hauteur
    public Pied(Integer hauteur){
    this.hauteur = hauteur;
    }

    }
    