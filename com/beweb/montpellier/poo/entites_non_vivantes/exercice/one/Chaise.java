
package com.beweb.montpellier.poo.entites_non_vivantes.exercice.one;

public class Chaise extends Meuble{

Boolean dossier;

// constructeur par défaut
public Chaise (){

}
// Constructeur de surcharge
public Chaise (Boolean dossier, String couleurMeuble, String nomMatiere, int taillePied, int nombrePied) {  
super(couleurMeuble, nomMatiere, taillePied, nombrePied);
this.dossier = dossier;
}

public boolean isTabouret(){
    if(this.dossier){
        return false;
    }
    return true;
}



// Méthode contenu() overridée
@Override
public void contenu(){
    System.out.println("La chaise est vide");
}

}