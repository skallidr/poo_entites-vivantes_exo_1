
package com.beweb.montpellier.poo.entites_non_vivantes.exercice.one;

public class Buffet extends Meuble{

    public Buffet(String couleurMeuble, String nomMatiere, int taillePied, int nombrePied){
    super(couleurMeuble, nomMatiere, taillePied, nombrePied);
}
// Méthode contenu() overridée
@Override
public void contenu(){
    System.out.println("La chaise est vide");
}
}