package com.beweb.montpellier.poo.entites_non_vivantes.exercice.one;

public class Banc extends Meuble{
    private int nombreAssises;
    
    // Constructeur par défaut$

    public Banc(){

}

    // Constructeur
public Banc (int nombreAssises, String couleurMeuble, String nomMatiere, int taillePied, int nombrePied){
    super(couleurMeuble, nomMatiere, taillePied, nombrePied);
    this.nombreAssises = nombreAssises;
}

  
   // constructeur si l'utilisateur ne spécifie pas le nombre de pieds
   public Banc (int nombreAssises, String couleurMeuble, String nomMatiere, int taillePied) {
       this(nombreAssises, couleurMeuble, nomMatiere, taillePied, 4);
   }
       




/**
 * @return the nombreAssises
 */
public int getNombreAssises() {
    return nombreAssises;
}
// Méthode contenu() overridée
@Override
public void contenu(){
    System.out.println("Le banc est vide");
}
}