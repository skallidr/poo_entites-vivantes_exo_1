
package com.beweb.montpellier.poo.entites_non_vivantes.exercice.one;

public class Matiere{

    // Propriétés de la class Matiere
    String nom;
    String couleur;
    
    // Constructeur par defaut
    public Matiere(){
    this.nom = "";
    this.couleur = "";
    }
    // Constructeur avec nom et couleur
    public Matiere(String nom, String couleur){
    this.nom = nom;
    this.couleur = couleur;
    
 
    }
    }
    