package com.beweb.montpellier.poo.entites_non_vivantes.exercice.one;

import java.util.ArrayList;

import com.beweb.montpellier.poo.entites_non_vivantes.exercice.one.Matiere;

public abstract class Meuble{
// Propiétés de la class Meuble
private ArrayList<Pied> listePieds;
private Matiere matiere;

// constructeur
public Meuble(){

}
// Constructeur
public Meuble(final ArrayList<Pied> listePieds, final Matiere matiere){
    this.listePieds = listePieds;
    this.matiere = matiere;
}

// Constructeur
public Meuble (final String couleurMeuble, final String nomMatiere, final int taillePied, final int nombrePied){
    this.matiere = new Matiere(nomMatiere, couleurMeuble);
    this.listePieds = new ArrayList<>();
    for(int i=0; i<nombrePied; i++){
        final Pied p = new Pied(taillePied);
        this.listePieds.add(p);
        // autre manière d'implémenter les deux lignes précedentes : this.listePieds.add(new Pied(taillePied));
    }
}

// Méthode d'affichage
@Override
        public String toString(){
            final int nbrePieds  = this.listePieds.size();
            final int xxcm = this.listePieds.get(1).hauteur;
            final String nomMatiere = this.matiere.nom;
            final String couleurMatiere = this.matiere.couleur;
            return "J'ai "+nbrePieds+" "+"pied(s) de"+" "+xxcm+" "+"de hauteur et je suis composé de"+" "+nomMatiere+" "+"de couleur"+" "+couleurMatiere;
           
        }

/**
 * @return the listePieds
 */
public ArrayList<Pied> getListePieds() {
    return listePieds;
}
/**
 * @return the matiere
 */
public Matiere getMatiere() {
    return matiere;
}

// Méthode contenu()
public abstract void contenu();
    

}



