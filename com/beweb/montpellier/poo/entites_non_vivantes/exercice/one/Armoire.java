package com.beweb.montpellier.poo.entites_non_vivantes.exercice.one;

public class Armoire extends Meuble{
    private int nombrePortes;

    // Constructeur
public Armoire(String couleurMeuble, String nomMatiere, int taillePied, int nombrePied, int nombrePortes){
    super(couleurMeuble, nomMatiere, taillePied,nombrePied);
    this.nombrePortes = nombrePortes;
 
}
/**
 * @return the nombrePortes
 */
public int getNombrePortes() {
    return nombrePortes;
}
// Méthode contenu() overridée
@Override
public void contenu(){
    System.out.println("L'armoire est vide");

}
}