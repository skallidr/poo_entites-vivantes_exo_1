package com.beweb.montpellier.poo.entites_vivantes.exercice.one;

public class Formateur extends Personne{

// Constructeur par défaut
public Formateur(){

}

// Constructeur de surcharge
public Formateur(String nom, String prenom){
    super(nom, prenom);
}

// Méthode pour afficher les propriétés de l'objet personne
public void afficheFormateur(){
    System.out.println(this.prenom+" "+this.nom);
    }
    @Override
    public String toString(){
        return "Moi";
    }

// Méthode pour noter un stagiaire
// 
public void noter(Stagiaire stagiaire, double note){
    stagiaire.setNotest(note);

}
public void calculerMoyenne(Stagiaire stagiaire){
    stagiaire.calculA();
}
}