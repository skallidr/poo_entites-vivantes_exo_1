package com.beweb.montpellier.poo.entites_vivantes.exercice.one;
import com.beweb.montpellier.poo.entites_non_vivantes.exercice.one.*;

public class Personne{
    // Propriétés de l'objet du type Personne
        String nom;
    String prenom;

    // Constructeur sans nom ni prenom
    public Personne(){
        this.nom = "";
        this.prenom = "";
        // autre possibilité de syntaxe pour le constructeur : this.nom = new String("");
    }

    // Constructeur avec nom et prenom
    public Personne(String n, String p){
        this.nom = n;
        this.prenom = p;
        // autre possibilité voir il est fortement conseillé que les paramètres passés en argument du même nom que les propriétés de l'objet. 
        // Par conséquent, la méthode constructeur peut se faire par : public Personne(String nom, String prenom){this.nom = nom;this.prenom = prenom}
    }
    
    // Méthode pour afficher les propriétés de l'objet personne
    public void affichePersonne(){
        System.out.println(this.prenom+" "+this.nom);
        }
        @Override
        public String toString(){
            return "Moi";
        }
    // Méthode observer()
    public void observer(Meuble meuble){
        meuble.contenu();
    }
    
    
}

