package com.beweb.montpellier.poo.entites_vivantes.exercice.one;
import java.util.ArrayList;


public class Eleve extends Personne{

ArrayList<Integer> listNotes;


// Constructeur par défaut
public Eleve(){

}

// Constructeur de surcharge
public Eleve(String nom, String prenom, Integer note1, Integer note2, Integer note3, Integer note4) {
    super(nom, prenom);
    this.listNotes = new ArrayList<Integer>();
    listNotes.add(note1);
    listNotes.add(note2);
    listNotes.add(note3);
    listNotes.add(note4);
    

}   
// Méthode pour remplir la liste de notes 
public void loadNotes(int n){    
    this.listNotes.add(n);
}


 // Méthode pour afficher les propriétés de l'objet eleve
 public void afficheEleve(){
    System.out.println(this.prenom+" "+this.nom+" "+this.listNotes);
    }
    @Override
    public String toString(){
        return "Moi";
    }

}