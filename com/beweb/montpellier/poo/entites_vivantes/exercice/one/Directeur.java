package com.beweb.montpellier.poo.entites_vivantes.exercice.one;

public class Directeur extends Personne{

// Constructeur par défaut
public Directeur(){

}

// Constructeur de surcharge
public Directeur(String nom, String prenom){
    super(nom, prenom);
}

// Méthode pour afficher les propriétés de l'objet personne
    public void afficheDirecteur(){
        System.out.println(this.prenom+" "+this.nom);
        }
        @Override
        public String toString(){
            return "Moi";
        }

}