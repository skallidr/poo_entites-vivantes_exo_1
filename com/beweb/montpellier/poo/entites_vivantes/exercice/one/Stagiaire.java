package com.beweb.montpellier.poo.entites_vivantes.exercice.one;

import java.util.ArrayList;


public class Stagiaire extends Personne{

ArrayList<Double> noteSt;

// Constructeur par défaut
public Stagiaire(){

}

// Constructeur de surcharge
public Stagiaire(String nom, String prenom){
    super(nom, prenom);
    this.noteSt = new ArrayList<Double>();
}

// Constructeur de surcharge
//public Stagiaire(String nom, String prenom) {
  //  super(nom, prenom);
    //this.noteSt = new ArrayList<Integer>();
   // noteSt.add(note1);
    //noteSt.add(note2);
    //noteSt.add(note3);
    //noteSt.add(note4);
   //}  



   // Méthode pour afficher les propriétés de l'objet personne
   public void afficheStagiaire(){
    System.out.println(this.prenom+" "+this.nom);
    }
    @Override
    public String toString(){
        return "Moi";
    }

// Méthode pour noter 
public void setNotest(double note){
    noteSt.add(note);
}

//Méthode affichage liste de notes
public void afficheNotes(){
    for(int i=0; i<noteSt.size(); i++){
        System.out.print(noteSt.get(i));
        System.out.print("; ");
    }
    System.out.println();

}

// Méthode pour noter 
public void calculA(){
    double sommeNotes = 0.0;
    for(Double note: noteSt) {
        sommeNotes = sommeNotes + note;
    }
    double average = sommeNotes/noteSt.size();
    System.out.println("La moyenne du stagiaire est: "+average);
}




}
