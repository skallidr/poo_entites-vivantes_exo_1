package com.beweb.montpellier.poo.entites_vivantes.exercice.one;
import com.beweb.montpellier.poo.entites_non_vivantes.exercice.one.*;

public class Main {
    
    public static void main(String[] args){
        System.out.println("Hello");
        //Personne greffier = new Personne("Jannot", "LAPIN");
        //Eleve e1 = new Eleve("Driss","SKALLI", 12, 15, 16, 11);
        //Chaise chaise1 = new Chaise (false, "Vengué", "Chene", 75, 4);
        //Table table1 = new Table("Beige", "Chene", 80, 4);
        //if (chaise1.isTabouret()){
            //System.out.println("Ceci est un tabouret");
            //}
            //else {System.out.println("Ceci est une chaise");}
        //greffier.observer(table1);
        //greffier.observer(chaise1);
        Directeur yoda = new Directeur("MAITRE JEDI", "Yoda");
        yoda.afficheDirecteur();
        Stagiaire anakin = new Stagiaire("SKYWALKER", "Anakin");
        anakin.afficheStagiaire();
        Formateur obiwan = new Formateur("KENOBI", "Obiwan");
        obiwan.afficheFormateur();
        obiwan.noter(anakin, 15.5);
        obiwan.noter(anakin, 12); 
        obiwan.noter(anakin, 10.75);
        obiwan.noter(anakin, 13);
        anakin.afficheNotes();
        obiwan.calculerMoyenne(anakin);
        //e1.afficheEleve();
        

        //e1.nom = "Eric";
        //p1.affichePersonne();
        //e1.afficheEleve();
    }
}